var selectedRow = null;

// clear fields
function clearFields() {
  document.querySelector("#menu").value = "";
  document.querySelector("#jml").value = "";
}

// submit menu
document.querySelector("#menu-form").addEventListener("submit", (e) => {
  e.preventDefault();

  const menu = document.querySelector("#menu").value;
  const jml = document.querySelector("#jml").value;

  if (selectedRow == null) {
    const list = document.querySelector("#menu-list");
    const row = document.createElement("tr");

    row.innerHTML = `
      <td>${menu}</td>
      <td>${jml}</td>
      <td>
        <a href="#" class="btn btn-info btn-sm edit">Edit</a>
        <a href="#" class="btn btn-danger btn-sm delete">Delete</a>
      </td>
    `;
    list.appendChild(row);
    selectedRow = null;
  } else {
    selectedRow.children[0].textContent = menu;
    selectedRow.children[1].textContent = jml;
    selectedRow = null;
  }
  clearFields();
});

// Edit Menu
document.querySelector("#menu-list").addEventListener("click", (e) => {
  target = e.target;
  if (target.classList.contains("edit")) {
    selectedRow = target.parentElement.parentElement;
    document.querySelector("#menu").value = selectedRow.children[0].textContent;
    document.querySelector("#jml").value = selectedRow.children[1].textContent;
  }
});

// Delete Menu
document.querySelector("#menu-list").addEventListener("click", (e) => {
  target = e.target;
  if (target.classList.contains("delete")) {
    target.parentElement.parentElement.remove();
  }
});
